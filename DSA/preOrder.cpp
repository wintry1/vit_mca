#include <iostream>
using namespace std;


struct tree{
    int data;
    struct tree *left,*right;
};

void insert(struct tree **root,int x)
{
    struct tree *p;
    p=(struct tree *)malloc(sizeof(struct tree));
    p->data=x;
    p->left=NULL;
    p->right=NULL;

    (*root)=p;
}

void pre(struct tree *root)
{
    if(root==NULL)
        return;
    cout<<root->data;
    pre(root->left);
    pre(root->right);
}

int main()
{
    struct tree *root=NULL;
    insert((&root),1);
    insert(&(root->left),2);
    insert(&(root->right),3);
    insert(&(root->left->left),4);
    insert(&(root->left->right),5);
    insert(&(root->right->left),6);
    insert(&(root->right->right),7);
    insert(&(root->left->left->left),8);
    insert(&(root->left->left->right),9);
    pre(root);
    return 0;

}
