import java.util.ArrayList;

interface CarbonFootprint {
    double getFootprint();
}

// Building class
class Building implements CarbonFootprint {
    String address;
    int floors;
    double area;

    Building(String address, int floors, double area) {
        this.address = address;
        this.floors = floors;
        this.area = area;
    }

    public double getFootprint() {
        // Calculating carbon footprint of building
        return (floors * area * 0.5); // Dummy calculation
    }

    public String toString() {
        return "Building: " + address;
    }
}

// Car class
class Car implements CarbonFootprint {
    String model;
    String make;
    int year;
    double mileage;

    Car(String model, String make, int year, double mileage) {
        this.model = model;
        this.make = make;
        this.year = year;
        this.mileage = mileage;
    }

    public double getFootprint() {
        // Calculating carbon footprint of car
        return (mileage * 0.5); // Dummy calculation
    }

    public String toString() {
        return "Car: " + make + " " + model + " (" + year + ")";
    }
}

// Bicycle class
class Bicycle implements CarbonFootprint {
    private String type;
    private String brand;
    private double weight;

    public Bicycle(String type, String brand, double weight) {
        this.type = type;
        this.brand = brand;
        this.weight = weight;
    }

    public double getFootprint() {
        // Calculating carbon footprint of bicycle
        return (weight * 0.1); // Dummy calculation
    }

    public String toString() {
        return "Bicycle: " + brand + " " + type;
    }
}



public class CarbonFootprintDemo {
    public static void main(String[] args) {
        ArrayList<CarbonFootprint> list = new ArrayList<CarbonFootprint>();

        // Creating objects of Building, Car and Bicycle
        Building building = new Building("123 Main St.", 5, 1000);
        Car car = new Car("Civic", "Honda", 2015, 30000);
        Bicycle bicycle = new Bicycle("Mountain", "Trek", 10);

        // Adding objects to ArrayList
        list.add(building);
        list.add(car);
        list.add(bicycle);

        // Iterating through ArrayList and calling getFootprint method
        for (CarbonFootprint item : list) {
            System.out.println(item.toString() + " - Carbon Footprint: " + item.getFootprint() + " kgCO2e");
        }
    }
}
