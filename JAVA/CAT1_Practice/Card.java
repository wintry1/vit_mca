class CARD {
    String cardno;
    String cust_name;
    String bank_name;

    public CARD(String cardno, String cust_name, String bank_name) {
        this.cardno = cardno;
        this.cust_name = cust_name;
        this.bank_name = bank_name;
    }
}

class Creditcard extends CARD {
    double limit;

    public Creditcard(String cardno, String cust_name, String bank_name, double limit) {
        super(cardno, cust_name, bank_name);
        this.limit = limit;
    }

    public void display() {
        System.out.println("Card Number: " + cardno);
        System.out.println("Customer Name: " + cust_name);
        System.out.println("Bank Name: " + bank_name);
        System.out.println("Credit Limit: " + limit);
    }

    public boolean use(double amount) {
        if (amount <= limit) {
            limit -= amount;
            System.out.println("Transaction Successful. Available limit: " + limit);
            return true;
        } else {
            System.out.println("Transaction Failed. Not enough credit limit available.");
            return false;
        }
    }
}

public class Card {
    public static void main(String[] args) {
        Creditcard[] cards = new Creditcard[2];
        cards[0] = new Creditcard("1234567890123456", "John Doe", "Bank of America", 5000);
        cards[1] = new Creditcard("9876543210987654", "Jane Doe", "Chase Bank", 10000);

        for (Creditcard card : cards) {
            System.out.println("\nCredit Card Details:");
            card.display();

            System.out.println("\nUsing the Credit Card:");
            card.use(2000);
            card.use(5000);
            card.use(4000);
        }
    }
}
