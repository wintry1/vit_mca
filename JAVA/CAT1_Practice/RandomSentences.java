import java.util.Random;

public class RandomSentences {
    public static void main(String[] args) {
        String[] article = {"the", "a", "one", "some", "any"};
        String[] noun = {"boy", "girl", "dog", "town", "car"};
        String[] verb = {"drove", "jumped", "ran", "walked", "skipped"};
        String[] preposition = {"to", "from", "over", "under", "on"};

        Random random = new Random();
        String[] sentence = new String[6];

        for (int i = 0; i < 20; i++) {
            // Select words at random from each array
            sentence[0] = article[random.nextInt(article.length)];
            sentence[1] = noun[random.nextInt(noun.length)];
            sentence[2] = verb[random.nextInt(verb.length)];
            sentence[3] = preposition[random.nextInt(preposition.length)];
            sentence[4] = article[random.nextInt(article.length)];
            sentence[5] = noun[random.nextInt(noun.length)];

            // Capitalize first word and add period at the end
            sentence[0] = sentence[0].substring(0, 1).toUpperCase() + sentence[0].substring(1);
            sentence[5] += ".";

            // Join words with spaces
            String randomSentence = String.join(" ", sentence);
            System.out.println(randomSentence);
        }
    }
}
