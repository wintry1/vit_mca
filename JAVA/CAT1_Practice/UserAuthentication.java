class AuthenticationExample extends Exception{
    public AuthenticationExample(String s){
        super(s);
    }
}

public class UserAuthentication{
    public static void authenticateUser(String username) throws AuthenticationExample{
        if(username.length()<6){
            throw new AuthenticationExample("Username should be atlease 6 characters!");
        }
    }
    public static void authenticatePassword(String pass) throws AuthenticationExample{
        if(!pass.equals("mypassword")){
            throw new AuthenticationExample("Password should match!");
        }
    }

    public static void main(String[] args) {
        String username="user";
        String pass="password";
    
        try{
            authenticateUser(username);
            authenticatePassword(pass);
        }
        catch(AuthenticationExample e){
            System.out.println(e.getMessage());
        }
        finally{
            System.out.println("Authentication Success!");
        }
    }
    
}

