package JAVA.CAT1_Ques;

import java.util.Scanner;

class ArraySort{
    int[] sortArray(int[] a)
    {
        for(int i=0;i<a.length;i++)
        {
            for(int j=i+1;j<a.length;j++)
            {
                if(a[i]>a[j])
                {
                    int temp=a[i];
                    a[i]=a[j];
                    a[j]=temp;
                }
            }
        }
        return a;
    }
    double[] sortArray(double[] a)
    {
        for(int i=0;i<a.length;i++)
        {
            for(int j=i+1;j<a.length;j++)
            {
                if(a[i]>a[j])
                {
                    double temp=a[i];
                    a[i]=a[j];
                    a[j]=temp;
                }
            }
        }
        return a;
    }
    
}


public class Q1{

    public static void main(String[] args) {
        ArraySort sortobb=new ArraySort();

        Scanner sc=new Scanner(System.in);

        System.out.println("How many elements: ");
        int n=sc.nextInt();

        int[] IntArr=new int[n];
        double[] DblArr=new double[n];
        String[] StrArray=new String[n];

        System.out.println("1. Integer\n2. Double\n3. String\n4.Exit\nEnter your choice: ");
        int ch=sc.nextInt();

        while(true)
        {
            switch(ch)
            {
                case 1: System.out.println("Enter elements:");
                        for(int i=0;i<IntArr.length;i++)
                            IntArr[i]=sc.nextInt();
                        sortobb.sortArray(IntArr);
                        for(int i=0;i<IntArr.length;i++)
                            System.out.println(IntArr[i]);
                        break;
                        
                case 2: System.out.println("Enter elements:");
                        for(int i=0;i<DblArr.length;i++)
                            DblArr[i]=sc.nextDouble();
                        sortobb.sortArray(DblArr);
                        for(int i=0;i<DblArr.length;i++)
                            System.out.println(DblArr[i]);
                        break;

                case 3: System.out.println("Enter elements:");
                        for(int i=0;i<StrArray.length;i++)
                            StrArray[i]=sc.nextLine();
                        sortobb.sortArray(StrArray);
                        for(int i=0;i<StrArray.length;i++)
                            System.out.println(StrArray[i]);
                        break;
            }
        }   
    }
}