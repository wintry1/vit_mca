import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Form extends JFrame implements ActionListener{
    Form(){
        JLabel l1=new JLabel("Enter your id: ");
        JLabel l2=new JLabel("Enter your password: ");
        JTextField id=new JTextField(25);
        JPasswordField pass=new JPasswordField(15);
        JButton submit=new JButton("Login");

        add(l1);
        add(id);
        add(l2);
        add(pass);
        add(submit);

        submit.addActionListener(this);

        setTitle("Login");
        setLayout(new FlowLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500,500);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFrame f=new JFrame("Welcome");

        JTextArea t1=new JTextArea(30,30);
        t1.setText("Welcome");
        t1.setEditable(false);

        JRadioButton b1=new JRadioButton("Yes");
        JRadioButton b2=new JRadioButton("No");

        f.add(t1);
        f.add(b1);
        f.add(b2);

        f.setLayout(new FlowLayout());
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(500,500);
        f.setVisible(true);

        
    }

    public static void main(String[] args) {
        new Form();
    }
    
}

