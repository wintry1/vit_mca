from pprint import pprint

n = int(input("Enter the value of n: "))
result = {}

for i in range(1, n+1):
    sum_i = (i * (i + 1)) // 2 
    divisors = []
    for j in range(1, sum_i+1):
        if sum_i % j == 0:
            divisors.append(j)
    result[sum_i] = divisors
    

result = dict(sorted(result.items()))

pprint(result)
