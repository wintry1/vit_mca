import csv
from pprint import pprint


def count_houses(file):
    with open(file, 'r', encoding='ISO-8859-1') as f:
        reader = csv.DictReader(f)
        freq_table = {}
        for row in reader:
            House = row['House']
            if House in freq_table:
                freq_table[House] += 1
            else:
                freq_table[House] = 1
    pprint(freq_table)

count_houses('/workspace/vit_mca/PYTHON/A3/character.csv')