def visited_cities(S, salesman1, salesman2, salesman3):
    visited = set(salesman1) | set(salesman2) | set(salesman3)
    unvisited = S - visited

    visited=list(visited)
    unvisited=list(unvisited)
    return visited, unvisited

S = {'Delhi', 'Mumbai', 'Kolkata', 'Chennai', 'Bangalore', 'Hyderabad', 'Ahmedabad', 'Pune', 'Jaipur'}
salesman1 = ['Mumbai', 'Kolkata', 'Chennai']
salesman2 = ['Hyderabad', 'Ahmedabad', 'Pune']
salesman3 = ['Jaipur', 'Chennai', 'Mumbai']

visited,unvisited=visited_cities(S, salesman1, salesman2, salesman3)
print("Visited: ", visited)
print("Not Visited: ", unvisited)