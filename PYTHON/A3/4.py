son_list = {"Milk", "Bread", "Butter", "Cheese", "Eggs", "Sugar"}
daughter_list = {"Milk", "Bread", "Butter", "Honey", "Jam", "Juice"}

common_items = son_list & daughter_list
son_only_items = son_list - daughter_list
daughter_only_items = daughter_list - son_list

final_list=common_items | son_only_items | daughter_only_items

print("Common Items: ", common_items)
print("Son Only Items: ", son_only_items)
print("Daughter Only Items: ", daughter_only_items)
print("Final List: ", final_list)
