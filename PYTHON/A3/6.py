def common_items(cust1, cust2):
    return tuple(set(cust1) & set(cust2))    

cust1 = ['apple', 'banana', 'chips','gauva','grapes','cold drinks']
cust2 = ['banana', 'watermelon', 'candy','apple','biscuits','gauva']
print("Common Items: ",common_items(cust1, cust2))