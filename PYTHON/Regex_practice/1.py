import re

#1
url="https:www.mit.edu"

protocol=re.findall("(\w+):", url)
hostname=re.findall(":www.([\w\-\.]+)", url)

print("Url: "+url)
print("Protocol: "+str(protocol))
print("Hostname: "+str(hostname))
print("==========================\n")

#2
url="file://localhost:4040/zip_file"

hostname=re.findall("://([\w]+)",url)
port=re.findall(":(\d+)", url)
p=port[0]
file_reg=p+"/(\w+)"
file=re.findall(file_reg, url)

print("Url: "+url)
print("Hostname: "+str(hostname))
print("Port: "+str(port))
print("File: "+str(file))
print("==========================\n")


#3
url="http://www.example.com/index.html"

protocol=re.findall("(\w+)://", url)
hostname=re.findall("www.([\w\-\.]+)", url)
page=re.findall(hostname[0]+"/([\w\-\.]+)", url)

print("Url: "+url)
print("Protocol: "+str(protocol))
print("Hostname: "+str(hostname))
print("Page: "+str(page))

