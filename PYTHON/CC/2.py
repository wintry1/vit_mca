import os
import re
from collections import defaultdict
from functools import reduce

def mapper(file_path):
    with open(file_path, 'r') as f:
        words = re.findall(r'\b\w+\b', f.read())
        word_counts = defaultdict(int)
        for word in words:
            word_counts[len(word)] += 1
        return list(word_counts.items())

def reducer(item1, item2):
    word_counts = defaultdict(int)
    for k, v in item1 + item2:
        word_counts[k] += v
    return list(word_counts.items())

if __name__ == '__main__':
    input_dir = '/workspace/vit_mca/PYTHON/CC/'
    file_paths = [os.path.join(input_dir, filename) for filename in os.listdir(input_dir) if filename.endswith('.txt')]
    word_counts_by_length = reduce(lambda x, y: reducer(x, y), map(mapper, file_paths))
    for word_length, count in sorted(word_counts_by_length):
        print(f"Words with length {word_length}: {count}")
