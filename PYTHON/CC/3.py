import os
import re
from collections import defaultdict
from functools import reduce

def map_func(file_path):
    with open(file_path) as f:
        text = f.read().lower()
        words = re.findall(r'\b\w+\b', text)
        anagrams = defaultdict(list)
        for word in words:
            key = ''.join(sorted(word))
            if key not in anagrams.keys():
                anagrams[key].append(word)
            if word not in anagrams[key]:
                anagrams[key].append(word)
        return list(anagrams.values())

def reduce_func(anagram_groups1, anagram_groups2):
    distinct_groups = []
    joined = anagram_groups1 + anagram_groups2
    for group in joined:
        if group not in distinct_groups:
            distinct_groups.append(group)
    return distinct_groups

def count_anagrams_in_documents(documents_dir):
    file_paths = [os.path.join(documents_dir, f) for f in os.listdir(documents_dir) if f.endswith('.txt')]
    anagram_groups = reduce(reduce_func, map(map_func, file_paths))
    for group in anagram_groups:
        if len(group) > 1:
            print("Words : ")
            for g in group:
                print(g)
            print("No. of occurrences : ", len(group), end="\n\n")

count_anagrams_in_documents('/workspace/vit_mca/PYTHON/CC/')
