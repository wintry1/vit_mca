import os
import re
from collections import Counter
from functools import reduce
from multiprocessing import Pool

def map_func(filename):
    with open(filename, 'r') as f:
        content = f.read()
    words = re.findall(r'\b\w+\b', content)
    return Counter(words)

def reduce_func(counter1, counter2):
    return counter1 + counter2

if __name__ == '__main__':
    folder_path = '/workspace/vit_mca/PYTHON/CC/'
    filenames = [os.path.join(folder_path, f) for f in os.listdir(folder_path) if f.endswith('.txt')]

    with Pool() as p:
        mapped = p.map(map_func, filenames)
    
    counts = reduce(reduce_func, mapped)

    for word, count in counts.items():
        print(f'{word}: {count}')
